//FOR LOOP
var nums = [1,2,3,4,5];
for(let i = 0;i<nums.length;i++){
    console.log(`nums[${i}] = ${nums[i]}`);
}
//FOR IN LOOP
var cars = {
    name: "BMW X7",
    company: "BMW",
    price: "1.18 Cr."
};
var str = '';
for(let x in cars){
    str += x + ' : ' +cars[x] + ' ';
}
console.log(str);
//FOR OF LOOP
for(let x of nums){
    console.log('number: '+ x);
}


//SWITCH CASE
var day = new Date().getDay();
switch(day){
    case 0:
        console.log("Today is SUNDAY 😊");
        break;
    case 6:
        console.log("Today is SATURDAY 😊");
        break;
    default:
        console.log("Not a weekend yet 😔");
}


//class
class person {
    constructor(Fname,Lname,age){
        this.Fname = Fname;
        this.Lname = Lname;
        this.age = age;
    }
    getName(){
        return this.Fname + ' ' + this.Lname;
    }
}
var p1 = new person("Tanmay","Maheshwari",21);
console.log('Name: ' + p1.getName());
console.log('Age: ' + p1.age);

//arrow function
var add = (a,b)=>a+b;
console.log(add(2,5));