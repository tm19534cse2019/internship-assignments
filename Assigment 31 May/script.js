let x = 3,y=4;
var add = function(x,y){
    return x+y;
}
var subtract = function(x,y){
    return x-y;
}
var multiply = function(x,y){
    return x*y;
}
var divide = function(x,y){
    return x/y;
}
var modulo = function(x,y){
    return x%y;
}
var power = function(x,y){
    return x**y;
}
document.querySelector('.add-fun').innerHTML = add(x,y);
document.querySelector('.subtract-fun').innerHTML = subtract(x,y);
document.querySelector('.multiply-fun').innerHTML = multiply(x,y);
document.querySelector('.divide-fun').innerHTML = divide(x,y);
document.querySelector('.remainder-fun').innerHTML = modulo(x,y);
document.querySelector('.power-fun').innerHTML = power(x,y);